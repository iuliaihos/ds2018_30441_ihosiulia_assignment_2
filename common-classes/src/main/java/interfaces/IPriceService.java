package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import entities.Car;

public interface IPriceService extends Remote{
	/**
	 * Computes the selling price for a Car.
	 *
	 * @param c Car for which to compute the selling price
	 * @return selling price for the car
	 */
	double computePrice(Car c) throws RemoteException;
}
