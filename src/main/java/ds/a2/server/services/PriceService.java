package ds.a2.server.services;

import java.rmi.RemoteException;

import ds.a2.common.entitites.Car;
import ds.a2.common.interfaces.IPriceService;


public class PriceService implements IPriceService{

	public double computePrice(Car c)  {
		if (c.getPurchasingPrice() <= 0) {
			throw new IllegalArgumentException("Purchasing price must be positive.");
		}
		int yearsTo2018 = 2018-c.getYear();
		if((yearsTo2018)>=7) 
			return c.getPurchasingPrice();
		return c.getPurchasingPrice()-((c.getPurchasingPrice()/7)*(yearsTo2018));
	}

}
