package views;

import java.awt.event.ActionListener;

import javax.swing.DropMode;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ClientView extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	/**
	 * @wbp.nonvisual location=177,459
	 */
	
	private JTextField engineSize;
	private JTextField fabricationYear;
	private JTextField purchasingPrice;
	private JButton btnComputeTax;
	private JButton btnComputeSellingPrice;
	private JTextArea textArea;

	public ClientView() {
		setTitle("RPC Application");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 652, 382);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("engine size");
		lblNewLabel.setBounds(25, 43, 116, 20);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("fabrication year");
		lblNewLabel_1.setBounds(25, 104, 126, 20);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("purchasing price");
		lblNewLabel_2.setBounds(25, 161, 116, 20);
		contentPane.add(lblNewLabel_2);
		
		engineSize = new JTextField();
		engineSize.setBounds(174, 40, 146, 26);
		contentPane.add(engineSize);
		engineSize.setColumns(10);
		
		fabricationYear = new JTextField();
		fabricationYear.setBounds(174, 101, 146, 26);
		contentPane.add(fabricationYear);
		fabricationYear.setColumns(10);
		
		purchasingPrice = new JTextField();
		purchasingPrice.setBounds(174, 158, 146, 26);
		contentPane.add(purchasingPrice);
		purchasingPrice.setColumns(10);
		
	    btnComputeTax = new JButton("Compute Tax");
		btnComputeTax.setBounds(404, 69, 157, 29);
		contentPane.add(btnComputeTax);
		
		btnComputeSellingPrice = new JButton("Compute Selling Price");
		btnComputeSellingPrice.setBounds(404, 136, 197, 29);
		contentPane.add(btnComputeSellingPrice);
		
		textArea = new JTextArea();
		textArea.setBounds(391, 230, 209, 60);
		contentPane.add(textArea);
	}
	
	public void addBtnComputeTaxActionListener(ActionListener e) {
		btnComputeTax.addActionListener(e);
	}
	
	public void addBtnComputePriceActionListener(ActionListener e) {
		btnComputeSellingPrice.addActionListener(e);
	}
	
	public void print(String message) {
		textArea.setText(message);
	}
	
	public void clear() {
		engineSize.setText("");
		purchasingPrice.setText("");
		fabricationYear.setText("");
	}
	

	public String getEngineSize() {
		return engineSize.getText();
	}

	public String getFabricationYear() {
		return fabricationYear.getText();
	}

	public String getPurchasingPrice() {
		return purchasingPrice.getText();
	}

}
