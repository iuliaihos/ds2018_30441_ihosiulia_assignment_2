package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import entities.Car;
import interfaces.IPriceService;
import interfaces.ITaxService;
import views.ClientView;

public class ClientController {
	private Registry registry;
	private ClientView view;

	public ClientController() {
		
		view = new ClientView();
		view.setVisible(true);
		
		try {
			registry = LocateRegistry.getRegistry();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		view.addBtnComputeTaxActionListener(new TaxActionListener());
	    view.addBtnComputePriceActionListener(new PriceActionListener());
	}
		class TaxActionListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				int engineSize = Integer.parseInt(view.getEngineSize());
				int year = Integer.parseInt(view.getFabricationYear());
				Car car = new Car(year, engineSize);
				ITaxService service;
				try {
					service = (ITaxService)registry.lookup("TaxService");
					System.out.println(car);
					double tax  = service.computeTax(car);
					view.print("Compted tax is " + tax );
				} catch (AccessException e1) {
					e1.printStackTrace();
				} catch (RemoteException e1) {
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					e1.printStackTrace();
				}
				
			}
			
		}
		
		class PriceActionListener implements ActionListener {

			public void actionPerformed(ActionEvent e) {
				int year = Integer.parseInt(view.getFabricationYear());
				double purchasingPrice = Double.parseDouble(view.getPurchasingPrice());
				Car car = new Car(year, purchasingPrice);
				
				IPriceService priceService;
				try {
					priceService = (IPriceService) registry.lookup("PriceService");	
					System.out.println(car);
					double price = priceService.computePrice(car);
					view.print("Computed selling price is " + price);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
				
				
			}
			
		}
		
		

}
