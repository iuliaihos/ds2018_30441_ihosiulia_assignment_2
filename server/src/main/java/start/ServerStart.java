package start;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import interfaces.IPriceService;
import interfaces.ITaxService;
import services.PriceService;
import services.TaxService;

public class ServerStart {

	public static void main(String[] args) {
		
		try {
			LocateRegistry.createRegistry(1099);
			ITaxService taxService = new TaxService();
			IPriceService priceService = new PriceService();
			Naming.rebind("TaxService", taxService);
			Naming.rebind("PriceService", priceService);
			System.out.println("The server started");
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}

}
