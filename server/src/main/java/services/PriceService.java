package services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import entities.Car;
import interfaces.IPriceService;



public class PriceService extends UnicastRemoteObject implements IPriceService{

	private static final long serialVersionUID = 1L;

	public PriceService() throws RemoteException {
		super();
	}

	public double computePrice(Car c) {
		if (c.getPurchasingPrice() <= 0) {
			throw new IllegalArgumentException("Purchasing price must be positive.");
		}
		int yearsTo2018 = 2018-c.getYear();
		if((yearsTo2018)>=7) 
			return c.getPurchasingPrice();
		return c.getPurchasingPrice()-((c.getPurchasingPrice()/7)*(yearsTo2018));
	}

}
